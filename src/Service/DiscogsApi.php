<?php

namespace Drupal\discogs_collection\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\TempStore\PrivateTempStore;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\discogs_collection\Form\AdminConfigForm;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

/**
 * Spotify Api Facade Service for command execution through Drupal.
 */
class DiscogsApi {

  use StringTranslationTrait;

  /**
   * Private tempstore collection.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected PrivateTempStore $collection;

  /**
   * A logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $loggerChannel;

  /**
   * The cache default service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  /**
   * A Guzzle Http Client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The modules config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  const API_URL = 'https://api.discogs.com';

  const REQUEST_HOST = 'api.discogs.com';

  const REQUEST_TOKEN_ENDPOINT = '/oauth/request_token';

  const ACCESS_TOKEN_ENDPOINT = '/oauth/access_token';

  const IDENTITY_ENDPOINT = '/oauth/identity';

  const PROFILE_ENDPOINT = '/users/{username}';

  const COLLECTION_VALUE_ENDPOINT = '/users/{username}/collection/value';

  const COLLECTION_FOLDERS_ENDPOINT = '/users/{username}/collection/folders';

  const COLLECTION_FOLDER_ENDPOINT = '/users/{username}/collection/folders/{folder_id}';

  const COLLECTION_FOLDER_RELEASES_ENDPOINT = '/users/{username}/collection/folders/{folder_id}/releases';

  const ARTIST_ENDPOINT = '/artists/{artist_id}';

  const RELEASE_ENDPOINT = '/releases/{release_id}';

  const INVENTORY_EXPORT = '/inventory/export';

  /**
   * Api service constructor.
   */
  public function __construct(
    Client $http_client,
    LoggerChannelFactoryInterface $logger_factory,
    PrivateTempStoreFactory $tempstore
  ) {
    $this->config = \Drupal::config(AdminConfigForm::SETTINGS);
    $this->httpClient = $http_client;
    $this->loggerChannel = $logger_factory->get('discogs_api');
    $this->collection = $tempstore->get('discogs_api');
  }

  /**
   * Requests an OAuth token from discogs.
   */
  public function requestOauthToken() {
    $data = $this->doRequest(static::REQUEST_TOKEN_ENDPOINT);
    if ($data) {
      $this->setPrivateOauthToken($data['oauth_token']);
      $this->setPrivateOauthTokenSecret($data['oauth_token_secret']);
      return new TrustedRedirectResponse('https://discogs.com/oauth/authorize?oauth_token=' . $data['oauth_token']);
    }
    return FALSE;
  }

  /**
   * Requests a personal access token from discogs.
   */
  public function requestAccessToken($verifier) {
    $data = $this->doRequest(static::ACCESS_TOKEN_ENDPOINT, ['verifier' => $verifier]);
    if ($data) {
      $this->setPrivateOauthToken($data['oauth_token']);
      $this->setPrivateOauthTokenSecret($data['oauth_token_secret']);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Requests identity information from discogs.
   */
  public function identity() {
    $data = $this->doRequest(static::IDENTITY_ENDPOINT);
    if ($data) {
      return $data;
    }
    return FALSE;
  }

  /**
   * Requests discogs profile data for logged in users.
   */
  public function profile() {
    $discogs_user = $this->getCurrentUserDiscogsUserName();
    if ($discogs_user) {
      $data = $this->doRequest(static::PROFILE_ENDPOINT, ['username' => $discogs_user]);
      if ($data) {
        return $data;
      }
    }
    return FALSE;
  }

  /**
   * Requests collection folders from discogs.
   */
  public function collectionFolders() {
    $discogs_user = $this->getCurrentUserDiscogsUserName();
    if ($discogs_user) {
      $data = $this->doRequest(static::COLLECTION_FOLDERS_ENDPOINT, ['username' => $discogs_user]);
      if ($data) {
        return $data;
      }
    }
    return FALSE;
  }

  /**
   * Requests collection all folder from discogs.
   */
  public function collectionFolderAll() {
    $discogs_user = $this->getCurrentUserDiscogsUserName();
    if ($discogs_user) {
      $page = 1;
      $data = [];
      $page_data = NULL;
      while ($page === 1 || $page_data && $page_data['pagination']['page'] < $page_data['pagination']['pages']) {
        $page_data = $this->doRequest(
          static::COLLECTION_FOLDER_RELEASES_ENDPOINT,
          [
            'username' => $discogs_user,
            'folder_id' => 0,
          ], [
            'page' => $page,
          ]
        );
        $data = array_merge($data, $page_data['releases']);
        $page++;
      }
      return $data;
    }
    return FALSE;
  }

  /**
   * Requests an artist from discogs.
   */
  public function artist($id) {
    $data = $this->doRequest(static::ARTIST_ENDPOINT, ['artist_id' => $id]);
    if ($data) {
      return $data;
    }
    return FALSE;
  }

  /**
   * Requests a release from discogs.
   */
  public function release($id) {
    $data = $this->doRequest(static::RELEASE_ENDPOINT, ['release_id' => $id]);
    if ($data) {
      return $data;
    }
    return FALSE;
  }

  /**
   * Requests the collection value from discogs.
   */
  public function collectionValue() {
    $discogs_user = $this->getCurrentUserDiscogsUserName();
    if ($discogs_user) {
      $data = $this->doRequest(static::COLLECTION_VALUE_ENDPOINT, ['username' => $discogs_user]);
      if ($data) {
        return $data;
      }
    }
    return FALSE;
  }

  public function exportInventory() {
    $data = $this->doRequest(static::INVENTORY_EXPORT);
    if ($data) {
      return $data;
    }
  }

  /**
   * Performs requests against discogs api.
   *
   * @param string $endpoint
   *   The endpoint to send requests against.
   * @param array $params
   *   Array of additional parameters that are needed for some endpoints.
   * @param array $query
   *   Array of query parameters to be attached to the endpoint's url.
   *
   * @return array|null
   *   Array of data from discogs or NULL if sth went wrong.
   */
  private function doRequest(string $endpoint, array $params = [], array $query = []): ?array {
    $method = 'GET';
    if (in_array($endpoint, [
      static::ACCESS_TOKEN_ENDPOINT,
      static::INVENTORY_EXPORT,
    ])) {
      $method = 'POST';
    }
    $original_endpoint = $endpoint;
    switch ($endpoint) {
      case static::REQUEST_TOKEN_ENDPOINT:
        $authorization_header = $this->buildAuthorizationHeader(NULL, NULL, NULL, \Drupal::request()->getSchemeAndHttpHost() . '/discogs-collection/verify');
        break;

      case static::ACCESS_TOKEN_ENDPOINT:
        $oauth_token = $this->getPrivateOauthToken();
        $oauth_token_secret = $this->getPrivateOauthTokenSecret();
        $authorization_header = $this->buildAuthorizationHeader($oauth_token, $oauth_token_secret, $params['verifier']);
        break;

      default:
        $authorization_header = $this->buildDefaultAuthorizationHeader();
        break;
    }

    $endpoint = $this->replaceEndpointParameters($endpoint, $params);
    $endpoint = $this->appendQueryParameters($endpoint, $query);

    try {
      $response = $this->httpClient->request(
        $method,
        static::API_URL . $endpoint,
        [
          RequestOptions::HEADERS => [
            'Host' => static::REQUEST_HOST,
            'User-Agent' => \Drupal::request()->headers->get('User-Agent', ''),
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Authorization' => $authorization_header,
            'Accept' => '*/*',
          ],
        ]
      );
      if ($response->getStatusCode() == 200) {
        $content = $response->getBody()->getContents();
        if (in_array($original_endpoint, [
          static::REQUEST_TOKEN_ENDPOINT,
          static::ACCESS_TOKEN_ENDPOINT,
        ])) {
          parse_str($content, $data);
        }
        else {
          $data = Json::decode($content);
        }
      }
    }
    catch (\Exception $e) {
      $this->loggerChannel->error($e->getMessage());
    }
    if (isset($data)) {
      return $data;
    }
    return NULL;
  }

  /**
   * Builds the default authorization header.
   */
  private function buildDefaultAuthorizationHeader(): string {
    $oauth_token = $this->getPrivateOauthToken();
    $oauth_token_secret = $this->getPrivateOauthTokenSecret();
    return $this->buildAuthorizationHeader($oauth_token, $oauth_token_secret);
  }

  /**
   * Builds the Authentication header to send with a request.
   */
  private function buildAuthorizationHeader($oauth_token = NULL, $oauth_token_secret = NULL, $verifier = NULL, $callback = NULL): string {
    $header = 'OAuth oauth_consumer_key="' . $this->config->get(AdminConfigForm::CONSUMER_KEY) . '"';
    $header .= ',oauth_signature_method="PLAINTEXT"';
    $header .= ',oauth_timestamp="' . \Drupal::time()->getCurrentTime() . '"';
    $header .= ',oauth_nonce="asdasdasdasd"';
    $header .= ',oauth_version="1.0"';
    if ($oauth_token) {
      $header .= ',oauth_token="' . $oauth_token . '"';
    }
    if ($oauth_token_secret) {
      $header .= ',oauth_signature="' . $this->config->get(AdminConfigForm::CONSUMER_SECRET) . '&' . $oauth_token_secret . '"';
    }
    else {
      $header .= ',oauth_signature="' . $this->config->get(AdminConfigForm::CONSUMER_SECRET) . '&"';
    }
    if ($verifier) {
      $header .= ',oauth_verifier="' . $verifier . '"';
    }
    if ($callback) {
      $header .= ',oauth_callback="' . $callback . '"';
    }
    return $header;
  }

  /**
   * Appends query parameters to a given url.
   */
  private function appendQueryParameters($url, array $query = []): string {
    if (count($query) > 0) {
      $first_item = TRUE;
      foreach ($query as $key => $value) {
        if ($first_item) {
          $url .= '?';
          $first_item = FALSE;
        }
        else {
          $url .= '&';
        }
        $url .= $key . '=' . $value;
      }
    }
    return $url;
  }

  /**
   * Replaces parameters inside the given url embraced by curly brackets.
   */
  private function replaceEndpointParameters($url, array $params = []): string {
    preg_match_all('/{(.*?)}/', $url, $matches);
    if (count($matches) > 0) {
      foreach ($matches[1] as $key => $value) {
        if (isset($params[$value])) {
          $url = str_replace($matches[0][$key], $params[$value], $url);
        }
      }
    }
    return $url;
  }

  /**
   * Get the discogs username from current user's field.
   */
  private function getCurrentUserDiscogsUserName() {
    if (\Drupal::currentUser()->isAuthenticated()) {
      $user_storage = \Drupal::entityTypeManager()->getStorage('user');
      $current_user = $user_storage->load(\Drupal::currentUser()->id());
      if ($current_user->hasField('field_discogs_user_name') && !$current_user->get('field_discogs_user_name')->isEmpty()) {
        return $current_user->get('field_discogs_user_name')->value;
      }
    }
    return NULL;
  }

  /**
   * Set the discogs username in current user's field.
   */
  public function setCurrentUserDiscogsUserName($username): void {
    if (\Drupal::currentUser()->isAuthenticated()) {
      $user_storage = \Drupal::entityTypeManager()->getStorage('user');
      $current_user = $user_storage->load(\Drupal::currentUser()->id());
      if ($current_user->hasField('field_discogs_user_name')) {
        $current_user->set('field_discogs_user_name', $username)->save();
      }
    }
  }

  /**
   * Sets private OAuth token.
   */
  private function setPrivateOauthToken($oauth_token): void {
    $this->collection->set('oauth_token', $oauth_token);
  }

  /**
   * Gets private OAuth token.
   */
  private function getPrivateOauthToken(): ?string {
    return $this->collection->get('oauth_token');
  }

  /**
   * Deletes private OAuth token.
   */
  private function deletePrivateOauthToken(): void {
    $this->collection->delete('oauth_token');
  }

  /**
   * Sets private OAuth token secret.
   */
  private function setPrivateOauthTokenSecret($oauth_token_secret): void {
    $this->collection->set('oauth_token_secret', $oauth_token_secret);
  }

  /**
   * Gets private OAuth token secret.
   */
  private function getPrivateOauthTokenSecret(): ?string {
    return $this->collection->get('oauth_token_secret');
  }

  /**
   * Deletes private OAuth token secret.
   */
  private function deletePrivateOauthTokenSecret(): void {
    $this->collection->delete('oauth_token_secret');
  }

}
