<?php

namespace Drupal\discogs_collection\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\TempStore\PrivateTempStore;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\discogs_collection\Service\DiscogsApi;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * OAuth Authentication Controller.
 */
class DiscogsApiController extends ControllerBase {

  /**
   * Private tempstore collection.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected PrivateTempStore $collection;

  /**
   * SpotifyApiAuthenticationController constructor.
   */
  public function __construct(
    protected DiscogsApi $discogsApi,
    PrivateTempStoreFactory $temp_store
  ) {
    $this->collection = $temp_store->get('discogs_api');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('discogs_collection.api'),
      $container->get('tempstore.private')
    );
  }

}
