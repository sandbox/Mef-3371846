<?php

namespace Drupal\discogs_collection\Controller;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * OAuth Authentication Controller.
 */
class AuthenticationController extends DiscogsApiController {

  use StringTranslationTrait;

  /**
   * Requests an Oauth Token.
   */
  public function requestOauthToken() {
    $this->collection->set('last_url', \Drupal::request()->server->get('HTTP_REFERER'));
    $response = $this->discogsApi->requestOauthToken();
    if ($response !== FALSE) {
      return $response;
    }
    else {
      return [
        '#markup' => $this->t('Something did not work as expected.'),
      ];
    }
  }

  /**
   * Requests an Access Token.
   */
  public function requestAccessToken() {
    parse_str(\Drupal::request()->getQueryString(), $oauth_params);
    $message = $this->t('Something went wrong during Discogs account verification.');
    $verified = $this->discogsApi->requestAccessToken($oauth_params['oauth_verifier']);
    if ($verified) {
      $message = $this->t('You have successfully verified your Discogs account.');
      $identity = $this->discogsApi->identity();
      $this->discogsApi->setCurrentUserDiscogsUserName($identity['username']);
    }
    if ($url = $this->collection->get('last_url')) {
      $this->collection->delete('last_url');
      \Drupal::messenger()->addMessage($message, $verified ? MessengerInterface::TYPE_STATUS : MessengerInterface::TYPE_ERROR);
      return new TrustedRedirectResponse($url);
    }
    return [
      '#markup' => $message,
    ];
  }

}
