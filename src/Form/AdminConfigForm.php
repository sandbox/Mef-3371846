<?php

namespace Drupal\discogs_collection\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure discogs collection admin settings.
 */
class AdminConfigForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'discogs_collection.admin_config';

  const FORM_ID = 'discogs_collection_admin_config';

  const CONSUMER_KEY = 'consumer_key';

  const CONSUMER_SECRET = 'consumer_secret';

  const REDIRECT_URL = 'redirect_url';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return static::FORM_ID;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    // API Credentials.
    $form += [
      'api_credentials' => [
        '#type' => 'details',
        '#title' => $this->t('API credentials'),
        '#open' => TRUE,
        '#description' => $this->t('Enter the <i>API credentials</i> for your Discogs App.'),
        static::CONSUMER_KEY => [
          '#type' => 'textfield',
          '#title' => $this->t('Consumer key'),
          '#required' => TRUE,
          '#description' => $this->t('Your consumer key.'),
          '#default_value' => $config->get(static::CONSUMER_KEY) ? $config->get(static::CONSUMER_KEY) : '',
        ],
        static::CONSUMER_SECRET => [
          '#title' => $this->t('Consumer secret'),
          '#type' => 'textfield',
          '#required' => TRUE,
          '#description' => $this->t('Your consumer secret.'),
          '#default_value' => $config->get(static::CONSUMER_SECRET) ? $config->get(static::CONSUMER_SECRET) : '',
        ],
        static::REDIRECT_URL => [
          '#title' => $this->t('Redirect url'),
          '#type' => 'url',
          '#required' => TRUE,
          '#description' => $this->t('Your apps redirect url.'),
          '#default_value' => $config->get(static::REDIRECT_URL) ? $config->get(static::REDIRECT_URL) : '',
        ],
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set(static::CONSUMER_KEY, $form_state->getValue(static::CONSUMER_KEY))
      ->set(static::CONSUMER_SECRET, $form_state->getValue(static::CONSUMER_SECRET))
      ->set(static::REDIRECT_URL, $form_state->getValue(static::REDIRECT_URL))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
